﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSAndLINQPractice
{
    interface IQueriable
    {
        void PrintDataHierarchically();
        void GetAmountOfUserTasksGroupedByProjects(int userId);
        void GetUserTasksWithNameLessThanFortyFiveChars(int userId);
        void GetTeamsWithUsersOlderThanTwelveYears();
        void GetUserFinishedInCurrentYearTasks(int userId);
        void GetSortedUsersWithTasks();
        void GetFirstMagicStruct(int userId);
        void GetSecondMagicStruct(int projectId);
    }
}
