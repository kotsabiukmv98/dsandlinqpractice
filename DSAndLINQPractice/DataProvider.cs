﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using DSAndLINQPractice.Models;
using System.Net.Http.Headers;
using Task = System.Threading.Tasks.Task;
using System.Linq;
using System.Collections;

namespace DSAndLINQPractice
{
    class DataProvider : IQueriable
    {
        private HttpClient _client = new HttpClient();
        private IList<Project> _projects;
        private IList<Models.Task> _tasks;
        private IList<TaskState> _taskStates;
        private IList<User> _users;
        private IList<Team> _teams;
        private string _baseUrl = "https://bsa2019.azurewebsites.net/";

        public DataProvider()
        {
            InitializeHttpClient();
            GetData().GetAwaiter().GetResult();
        }

        //  Відобразити отримані дані у вигляді набору сутностей (вкладених об'єктів).
        // Project
        //      Tasks
        //          Performer (User)
        //      Author
        //      Team
        public void PrintDataHierarchically()
        {
            var projects = _projects.Select(p => new
            {
                Project = p,
                Tasks = _tasks.Where(t => t.ProjectId == p.Id).Select(t => new
                {
                    Task = t,
                    Performer = _users.FirstOrDefault(u => u.Id == t.PerformerId)
                }),
                Author = _users.FirstOrDefault(u => u.Id == p.AuthorId),
                Team = _teams.FirstOrDefault(t => t.Id == p.TeamId)
            }).ToList();

            var i = 1;
            foreach(var project in projects)
            {
                Console.WriteLine($"{i.ToString()}. {project.Project.ToString()}");
                Console.WriteLine("\tTASKS:");
                var j = 1;
                foreach(var task in project.Tasks)
                {
                    Console.WriteLine($"\t{j.ToString()}. {task.Task.ToString()}");
                    Console.WriteLine("\t\tPERFORMER:");
                    Console.WriteLine($"\t\t{task.Performer.ToString()}\n");
                    ++j;
                }
                Console.WriteLine($"\n\tAUTHOR");
                Console.WriteLine($"\t{project.Author.ToString()}");
                Console.WriteLine($"\n\tTEAM");
                Console.WriteLine($"\t{project.Team.ToString()}\n");
                ++i;
            }
        }

        //1. Отримати кількість тасків у проекті конкретного користувача (по id)
        public void GetAmountOfUserTasksGroupedByProjects(int userId)
        {
            var result = _tasks
                .Where(t => t.PerformerId == userId)
                .GroupBy(t => t.ProjectId)
                .ToDictionary(g => _projects.FirstOrDefault(proj => proj.Id == g.Key), g => g.Count());

            if (result.Count == 0)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            int i = 1;
            Console.WriteLine("\nRESULT:\n");
            foreach (var pair in result)
            {
                Console.WriteLine($"{i.ToString()}. {pair.Key.ToString()}");
                Console.WriteLine($"\tAmount of tasks: {pair.Value.ToString()}\n");
                ++i;
            }
            Console.WriteLine();
        }

        //2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів.
        public void GetUserTasksWithNameLessThanFortyFiveChars(int userId)
        {
            var result = _tasks
                .Where(t => t.PerformerId == userId && t.Name.Length < 45)
                .ToList();

            int i = 1;
            if (result.Count == 0)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            Console.WriteLine("\nRESULT:\n");
            foreach (var task in result)
            {
                Console.WriteLine($"{i.ToString()}. {task.ToString()}");
                ++i;
            }
            Console.WriteLine();
        }

        //3. Отримати список (id, name) з колекції тасків, які виконані в поточному році для конкретного користувача (по id)
        public void GetUserFinishedInCurrentYearTasks(int userId)
        {
            Func<Models.Task, bool> predicate = task =>
            {
                int currentYear = DateTime.Now.Year;
                return
                    task.PerformerId == userId &&
                    _taskStates.FirstOrDefault(state => state.Id == task.State).Value == "Finished" &&
                    task.FinishedAt.Year == currentYear;
            };
            var result = _tasks
                    .Where(predicate)
                    .Select(t => new { t.Id, t.Name })
                    .ToList();

            int i = 1;
            if (result.Count == 0)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            Console.WriteLine("\nRESULT:\n");
            foreach (var task in result)
            {
                Console.WriteLine($"{i.ToString()}. {task.Name} TASK with Id '{task.Id }'");
                ++i;
            }
            Console.WriteLine();
        }

        //4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 12 років,
        //   відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        public void GetTeamsWithUsersOlderThanTwelveYears()
        {
            var result = (from team in _teams
                          join user in _users.Where(u => DateTime.Now.Year - u.Birthday.Year > 12)
                          on team.Id equals user.TeamId
                          select new { team.Id, User = user } into teams
                          group teams by teams.Id into groupedUsers
                          select new
                          {
                              Id = groupedUsers.Key,
                              Users = groupedUsers.ToList(),
                              _teams.FirstOrDefault(t => t.Id == groupedUsers.Key).Name
                          }
                         ).ToList();

            if (result.Count() == 0)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            int i = 1;
            Console.WriteLine("\nRESULT:\n");
            foreach (var team in result)
            {
                Console.WriteLine($"{i.ToString()}. {team.Name} TEAM with Id '{team.Id }' has users:");
                var j = 1;
                foreach (var user in team.Users)
                {
                    Console.WriteLine($"\t{j.ToString()}. {user.ToString()}");
                    ++j;
                }
                ++i;
            }
            Console.WriteLine();
        }

        //5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks
        //   по довжині name (за спаданням).
        public void GetSortedUsersWithTasks()
        {
            var result = _users
                .OrderBy(u => u.FirstName)
                .Select(u => new
                {
                    User = u,
                    Tasks = _tasks
                        .Where(t => t.PerformerId == u.Id)
                        .OrderByDescending(t => t.Name.Length)
                        .ToList()
                }).ToList();

            if (result.Count() == 0)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            int i = 1;
            Console.WriteLine("\nRESULT:\n");
            foreach (var user in result)
            {
                Console.WriteLine($"{i.ToString()}. {user.User.ToString()}");
                var j = 1;
                foreach (var task in user.Tasks)
                {
                    Console.WriteLine($"\t{j.ToString()}. {task.ToString()}");
                    ++j;
                }
                ++i;
            }
            Console.WriteLine();
        }

        //6. Отримати наступну структуру (передати Id користувача в параметри):
        //      User
        //      Останній проект користувача(за датою створення)
        //      Загальна кількість тасків під останнім проектом
        //      Загальна кількість незавершених або скасованих тасків для користувача
        //      Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
        //      P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        //P.S. ВИБАЧТЕ ЗА НАЗВУ МЕТОДА:)
        public void GetFirstMagicStruct(int userId)
        {
            var userInfo = (from user in _users
                            where user.Id == userId
                            let lastProject = _projects.Where(p => p.AuthorId == user.Id).OrderByDescending(p => p.CreatedAt).FirstOrDefault()
                            let tasks = _tasks.Where(t => t.PerformerId == user.Id)
                            select new
                            {
                                User = user,
                                LastProject = lastProject,
                                AmountOfTasks = tasks
                                    .Where(t => t.ProjectId == lastProject?.Id)
                                    .Count(),
                                AmountOfNotFinishedTasks = tasks
                                    .Where(t => _taskStates.FirstOrDefault(ts => ts.Id == t.State).Value != "Finished")
                                    .Count(),
                                LongestTask = tasks
                                    .OrderByDescending(t => t.CreatedAt - t.FinishedAt)
                                    .FirstOrDefault()
                            }).FirstOrDefault();
            if (userInfo == null)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            Console.WriteLine(userInfo.User.ToString());
            Console.WriteLine($"Last project:\n\t{userInfo.LastProject?.ToString() ?? "User does not have any projects"}");
            Console.WriteLine($"Amount of last project's tasks :\n\t{userInfo.AmountOfTasks.ToString()}");
            Console.WriteLine($"Amount of not finished and canceled tasks:\n\t{userInfo.AmountOfNotFinishedTasks.ToString()}");
            Console.WriteLine($"The longest task:\n\t{userInfo.LongestTask.ToString()}\n");
        }

        //7. Отримати таку структуру (передати Id проекту в параметри)
        //      Проект
        //      Найдовший таск проекту(за описом)
        //      Найкоротший таск проекту(по імені)
        //      Загальна кількість користувачів в команді проекту, де або опис проекту >25 символів, або кількість тасків<3
        // P.S. ВИБАЧТЕ ЗА НАЗВУ МЕТОДА ЩЕ РАЗ :)
        public void GetSecondMagicStruct(int projectId)
        {
            var projectInfo = (from project in _projects
                               where project.Id == projectId
                               let tasks = _tasks.Where(t => t.ProjectId == project.Id)
                               let team = _teams.FirstOrDefault(t => t.Id == project.TeamId)
                               select new
                               {
                                   Project = project,
                                   LongestTask = tasks
                                        .OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                                   ShortestTask = tasks
                                        .OrderBy(t => t.Name).FirstOrDefault(),
                                   AmountOfUsers = _projects.GroupJoin(_tasks, p => p.Id, t => t.ProjectId, (project, task) => new
                                                   {
                                                       project.Id,
                                                       project.Description,
                                                       TaskCount = task.Count()
                                                   }).Where(p => (p.TaskCount < 3 && p.TaskCount != 0) || (p.Description.Length > 25))
                                                    .Select(p => _tasks.FirstOrDefault(t => t.ProjectId == p.Id))
                                                    .Select(t => _users.FirstOrDefault(u => u.Id == t?.PerformerId))
                                                    .GroupBy(u => u?.Id)
                                                    .Count()
                                }).FirstOrDefault();
            if (projectInfo == null)
            {
                Console.WriteLine("\nRESULT IS EMPTY!\n");
                return;
            }
            Console.WriteLine(projectInfo.Project.ToString());
            Console.WriteLine($"Longest task by description:\n\t{projectInfo.LongestTask?.ToString() ?? "Project does not have any tasks"}");
            Console.WriteLine($"Shortest task by name:\n\t{projectInfo.ShortestTask?.ToString() ?? "Project does not have any tasks"}");
            Console.WriteLine($"Amount of user in project's team\n\t{projectInfo.AmountOfUsers.ToString()}\n");
        }

        private async Task GetData()
        {
            HttpResponseMessage responce = await _client.GetAsync(_baseUrl + "api/Projects");
            if (responce.IsSuccessStatusCode)
            {
                Console.WriteLine("Data loading 1/5. Wait, please...");
                _projects = await responce.Content.ReadAsAsync<IList<Project>>();
            }

            responce = await _client.GetAsync(_baseUrl + "api/Tasks");
            if (responce.IsSuccessStatusCode)
            {
                Console.WriteLine("Data loading 2/5. Wait, please...");
                _tasks = await responce.Content.ReadAsAsync<IList<Models.Task>>();
            }

            responce = await _client.GetAsync(_baseUrl + "api/Teams");
            if (responce.IsSuccessStatusCode)
            {
                Console.WriteLine("Data loading 3/5. Wait, please...");
                _teams = await responce.Content.ReadAsAsync<IList<Team>>();
            }

            responce = await _client.GetAsync(_baseUrl + "api/TaskStates");
            if (responce.IsSuccessStatusCode)
            {
                Console.WriteLine("Data loading 4/5. Wait, please...");
                _taskStates = await responce.Content.ReadAsAsync<IList<TaskState>>();
            }

            responce = await _client.GetAsync(_baseUrl + "api/Users");
            if (responce.IsSuccessStatusCode)
            {
                Console.WriteLine("Data loading 5/5. Wait, please...");
                _users = await responce.Content.ReadAsAsync<IList<User>>();
            }
            Console.WriteLine("DONE!\n");
        }
        private void InitializeHttpClient()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
