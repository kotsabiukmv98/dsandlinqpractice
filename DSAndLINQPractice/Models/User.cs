﻿using Newtonsoft.Json;
using System;

namespace DSAndLINQPractice.Models
{
    class User
    {
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        [JsonProperty("registered_at")]
        public DateTime RegistredAt { get; set; }
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }
        public DateTime Birthday { get; set; }
        public override string ToString()
        {
            string teamId = TeamId.HasValue ?
                $"Team Id '{TeamId.Value.ToString()}'" :
                "User does not have a team";
            return $"{FirstName} {LastName} USER with Id '{Id}' registered at {RegistredAt.ToString()}. {teamId}";
        }
    }
}
