﻿using Newtonsoft.Json;
using System;

namespace DSAndLINQPractice.Models
{
    class Team
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt{ get; set; }
        public override string ToString()
        {
            return $"{Name} TEAM with Id '{Id}' created at {CreatedAt}";
        }
    }
}
