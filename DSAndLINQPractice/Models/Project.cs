﻿using Newtonsoft.Json;
using System;

namespace DSAndLINQPractice.Models
{
    class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        public DateTime Dedline { get; set; }
        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }

        public override string ToString()
        {
            return $"'{Name}' PROJECT with Id '{Id}' created at {CreatedAt.ToString()}";
        }
    }
}
