﻿namespace DSAndLINQPractice.Models
{
    class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return Value;
        }
    }
}
