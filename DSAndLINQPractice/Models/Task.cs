﻿using Newtonsoft.Json;
using System;

namespace DSAndLINQPractice.Models
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }
        public override string ToString()
        {
            return $"'{Name}' TASK with Id '{Id}' created at {CreatedAt.ToString()}. Current state is '{State}'. Project id '{ProjectId}'. Performer Id '{PerformerId}'";
        }
    }
}
