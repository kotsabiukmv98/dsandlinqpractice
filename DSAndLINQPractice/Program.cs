﻿using System;
using static DSAndLINQPractice.ConsoleHelper;

namespace DSAndLINQPractice
{
    class Program
    {
        static IQueriable _dataProvider;

        static void Main(string[] args)
        {
            _dataProvider = new DataProvider();

            while(true)
            {
                int actionCode = PromptAction();
                InvokeAction(actionCode);
                Console.WriteLine("\nPress any key to continue...\n");
                Console.ReadKey();
            }
        }
        static void InvokeAction(int actionCode)
        {
            switch (actionCode)
            {
                case 1:
                    GetAmountOfUserTasksGroupedByProjects();
                    break;
                case 2:
                    GetUserTasksWithNameLessThanFortyFiveChars();
                    break;
                case 3:
                    GetUserFinishedInCurrentYearTasks();
                    break;
                case 4:
                    GetTeamsWithUsersOlderThanTwelveYears();
                    break;
                case 5:
                    GetSortedUsersWithTasks();
                    break;
                case 6:
                    GetFirstMagicStruct();
                    break;
                case 7:
                    GetSecondMagicStruct();
                    break;
                case 8:
                    PrintDataHierarchically();
                    break;
                case 9:
                    Exit();
                    break;
                default:
                    break;
            }
        }
        static void GetAmountOfUserTasksGroupedByProjects()
        {
            int userId = PromptInt(nameof(userId));
            _dataProvider.GetAmountOfUserTasksGroupedByProjects(userId);
        }
        static void GetUserTasksWithNameLessThanFortyFiveChars()
        {
            int userId = PromptInt(nameof(userId));
            _dataProvider.GetUserTasksWithNameLessThanFortyFiveChars(userId);
        }
        static void GetUserFinishedInCurrentYearTasks()
        {
            int userId = PromptInt(nameof(userId));
            _dataProvider.GetUserFinishedInCurrentYearTasks(userId);
        }
        static void GetTeamsWithUsersOlderThanTwelveYears()
        {
            _dataProvider.GetTeamsWithUsersOlderThanTwelveYears();
        }
        static void GetSortedUsersWithTasks()
        {
            _dataProvider.GetSortedUsersWithTasks();
        }
        static void GetFirstMagicStruct()
        {
            int userId = PromptInt(nameof(userId));
            _dataProvider.GetFirstMagicStruct(userId);
        }
        static void GetSecondMagicStruct()
        {
            int projectId = PromptInt(nameof(projectId));
            _dataProvider.GetSecondMagicStruct(projectId);
        }
        static void PrintDataHierarchically()
        {
            _dataProvider.PrintDataHierarchically();
        }
        static void Exit()
        {
            Environment.Exit(0);
        }
    }
}
