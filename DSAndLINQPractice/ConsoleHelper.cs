﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DSAndLINQPractice
{
    static class ConsoleHelper
    {
        public static int PromptAction()
        {
            var actionCode = 0;
            while (actionCode < 1 || actionCode > 9)
            {
                Console.Clear();
                Console.WriteLine("Choose action by typing number and ENTER:");
                Console.WriteLine("\t1. Get amount of user's tasks grouped by projects.");
                Console.WriteLine("\t2. Get user's tasks with name less than 45 characters.");
                Console.WriteLine("\t3. Get user's tasks finished in current year.");
                Console.WriteLine("\t4. Get teams with users older than twelve years.");
                Console.WriteLine("\t5. Get sorted users with tasks.");
                Console.WriteLine("\t6. Get first magic structure.");
                Console.WriteLine("\t7. Get second magic structure.");
                Console.WriteLine("\t8. Print data hierarchically.");
                Console.WriteLine("\t9. Exit.");
                var input = Console.ReadLine();
                int.TryParse(input, out actionCode);
            }
            return actionCode;
        }

        public static int PromptInt(string valueName)
        {
            Console.WriteLine($"Type '{valueName}' value, please");
            string input = Console.ReadLine();
            int userId;
            bool correctValue = int.TryParse(input, out userId);
            while (!correctValue)
            {
                Console.WriteLine($"Incorrect value, type '{valueName}' again, please");
                input = Console.ReadLine();
                correctValue = int.TryParse(input, out userId);
            }
            return userId;
        }
    }
}
